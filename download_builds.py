import os
import subprocess

### Define the bucket name and build directory
# GCP BucketName
BUCKET_NAME = "build-artifacts.five9.net"
# Bucket Folder Hosting SCC Builds
BUILD_DIR = "SCC/PATCH/2024"
# Bucket Folder Hosting SA Builds
SA_BUILD_DIR = "rel13.0/patch/social-admin"
# Local mount hosting Builds
TARGET_FOLDER="/builds/rel13.0/"
#TARGET_FOLDER="./builds/"
# How many of the latest builds will be downloaded
latest_Nth_builds = 10
# Set the path to the service account key file
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "devops-build-artifact-reader@XXXXXXXX.gserviceaccount.com"
# Remote Util Hosts to deliver downloaded builds
remote_util_hosts=["util001.atl.five9.com","util001.ldn.five9.com"]
command = 'sudo gcloud auth activate-service-account --key-file=/path/to/serviceaccount.json'
process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
output, error = process.communicate()

print(output.decode())



# Download and sort SCC builds
print("Downloading and sorting SCC builds...")
#python3.7
#SCC_BUILDS = subprocess.check_output(f"gsutil ls gs://{BUCKET_NAME}/{BUILD_DIR}/SCC.*.tgz | grep -v '.md5' | sort -t '-' -k 2,2nr -k 3,3nr -k 4,4nr | head -{latest_Nth_buils}", shell=True, text=True).splitlines()
#python3.6
SCC_BUILDS = subprocess.check_output(f"gsutil ls gs://{BUCKET_NAME}/{BUILD_DIR}/SCC.*.tgz | grep -v '.md5' | sort -t '-' -k 2,2nr -k 3,3nr -k 4,4nr | head -{latest_Nth_builds}", shell=True, universal_newlines=True).splitlines()
# Download and sort SCC_SERVICES builds
print("Downloading and sorting SCC_SERVICES builds...")
#python3.7
#SCC_SERVICES_BUILDS = subprocess.check_output(f"gsutil ls gs://{BUCKET_NAME}/{BUILD_DIR}/SCC_SERVICES*.tgz | grep -v '.md5' | sort -t '-' -k 2,2nr -k 3,3nr -k 4,4nr | head -{latest_Nth_builds}", shell=True, text=True).splitlines()
#python3.6
SCC_SERVICES_BUILDS = subprocess.check_output(f"gsutil ls gs://{BUCKET_NAME}/{BUILD_DIR}/SCC_SERVICES*.tgz | grep -v '.md5' | sort -t '-' -k 2,2nr -k 3,3nr -k 4,4nr | head -{latest_Nth_builds}", shell=True, universal_newlines=True).splitlines()
SA_BUILDS = subprocess.check_output(f"gsutil ls gs://{BUCKET_NAME}/{SA_BUILD_DIR}/social-admin*.tgz | grep -v '.md5' | sort -V | tail -{latest_Nth_builds}", shell=True, universal_newlines=True).splitlines()

# Download the top Nth SCC builds
for SCC_BUILD in SCC_BUILDS:
    print(f"Downloading {SCC_BUILD}...")
    subprocess.run(f"sudo gsutil cp {SCC_BUILD} {TARGET_FOLDER}", shell=True)
    print(f"__________________{TARGET_FOLDER}{SCC_BUILD}________________")
# Also deliver builds to other DC util servers
    for host in remote_util_hosts:
        subprocess.run(f"sudo scp {os.path.basename(SCC_BUILD)} {host}:{TARGET_FOLDER}", shell=True)
        print(f"******** Copied {os.path.basename(SCC_BUILD)} to remote {host}:{TARGET_FOLDER} *************")

# Download the top Nth SCC_SERVICES builds
for SCC_SERVICES_BUILD in SCC_SERVICES_BUILDS:
    print(f"Downloading {SCC_SERVICES_BUILD}...")
    subprocess.run(f"sudo gsutil cp {SCC_SERVICES_BUILD} {TARGET_FOLDER}", shell=True)
# Also deliver builds to other DC util servers
    for host in remote_util_hosts:
        subprocess.run(f"sudo scp {os.path.basename(SCC_SERVICES_BUILD)} {host}:{TARGET_FOLDER}", shell=True)
        print(f"******** Copied {os.path.basename(SCC_SERVICES_BUILD)} to remote {host}:{TARGET_FOLDER} *************")

# Download the top Nth SCC_SERVICES builds
for SA_BUILD in SA_BUILDS:
    print(f"Downloading {SA_BUILD}...")
    subprocess.run(f"sudo gsutil cp {SA_BUILD} {TARGET_FOLDER}", shell=True)
# Also deliver builds to other DC util servers
    for host in remote_util_hosts:
        subprocess.run(f"sudo scp {os.path.basename(SA_BUILD)} {host}:{TARGET_FOLDER}", shell=True)
        print(f"******** Copied {os.path.basename(SA_BUILD)} to remote {host}:{TARGET_FOLDER} *************")

print("Done.")
username = "root"
groupname ="root"
for root,dirs,files in os.walk(TARGET_FOLDER):
  for file in files:
    if file in SCC_BUILDS or SCC_SERVICES_BUILDS or SA_BUILDS:
      file_path = os.path.join(root,file)
      subprocess.call(["sudo", "chown", username + ":" + groupname, file_path])